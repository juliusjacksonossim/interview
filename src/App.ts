import express, { Request, Response } from "express";
import cors from "cors";
import bodyParser from "body-parser";

const app = express();

app.use(cors());
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));



app.get("/:n/:m", async (req: Request, res: Response) => {
    const n: number = parseInt(req.params.n);
    const m: number = parseInt(req.params.m);
    if (n && m) {
        try {
            const result: number = await (n + m);

            res.status(200).send(result);
        } catch (e) {
            res.status(404).send(e.message);
        }
    }


});


app.listen(8000,()=>{
    console.log('Server Started at Port, 8000')
});